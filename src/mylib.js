/** Basic arithmetich operations */

const mylib = {
    /**Multiline arrow function.  */
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    subtract: (a, b) => {
        return a - b;
    },
    /**Single line arrow function */
    divide: (dividend, divider) => {
        if(divider === 0) {
            return "can not divide with 0";
        }
        return dividend / divider;
    },
    
    /** Regular function */
    multiply: function(a, b) {
        return a*b;
    },
        

};

module.exports = mylib;