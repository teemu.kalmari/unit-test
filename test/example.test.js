const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit test", () => {
    before(() => {
        //initialization
        //create objects:::
        console.log("Initializing completed.");
    })
    it("Can add 1 and 2 together", () => {
        expect(mylib.add(1,2)).equal(3, "1+2 is not 3, for some reason?");
    });
    it("Can substract 1 from 2.", () => {
        expect(mylib.subtract(2,1)).equal(1, "2-1 is not 1, for some reason?");
    });
    it("Can divide 4 with 2.", () => {
        expect(mylib.divide(4,2)).equal(2, "4/2 is not 2, for some reason?");
    });
    it("Can multiply 2 with 2.", () => {
        expect(mylib.multiply(2,2)).equal(4, "2*2 is not 4, for some reason?");
    });

    after(() => {
        //cleanup
        //FOR example: shutdown the Express server.
        console.log("Testing completed!")
    })
});
describe("ZeroDivision test", () => {
    before(() => {
        //initialization
        //create objects:::
        console.log("Initializing completed.");
    })
  
   
    it("Test when divider is 0.", () => {
        expect(mylib.divide(4,0)).equal("dividor is 0");
    });
   
    after(() => {
        //cleanup
        //FOR example: shutdown the Express server.
        console.log("Testing completed!")
    })
});